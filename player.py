from subprocess import  Popen
from chario import getUpDown

class Player:
  def __init__(self, mac):
    if mac:
      self.playerName = '/Applications/VLC.app/Contents/MacOS/VLC'
    else:
      self.playerName = 'cvlc'
    self.start() 
    self.channel = None
  
  def start(self):
    self.vlc = Popen([self.playerName])
    
  def stop(self):
    self.vlc.kill()
    self.channel = None
  
  def play(self, channel):
    self.stop()
    if hasattr(channel, 'stream'):
      print "Playing " + channel.name 
      self.channel = channel
      self.vlc = Popen([self.playerName, channel.stream])
  
  def playNext(self):  
     if self.channel != None:
       self.channelManager.nextChannel(self.channel)
       print "shift up "

  def playPrevious(self):  
     if self.channel != None:
       self.channelManager.nextChannel(self.channel)
       print "shift down "

  def doControl(self):
    direction = getUpDown()
    if direction == +1:
      Popen (["./vol.sh", "+"])
    if direction == -1:
      Popen (["./vol.sh", "-"])
    #if direction == 2:
    #  self.playNext()
    #if direction == -2:
    #  self.playPrevious()  