import yaml

class Channel:
  def __init__(self,  **channelSpec):
    self.__dict__.update(channelSpec)
  
class ChannelManager:
  def __init__(self):
    self.theChannels = {}
    f = open ('channels.yaml')    
    channelSpec = yaml.load(f)
    self.__dict__.update(**channelSpec)
    for channelSpec in self.channels:
      channel = Channel(**channelSpec)
      self.theChannels[channel.key] = channel

  def lookupChannel(self, key):
    if key in self.theChannels:
      return self.theChannels[key]
    else:
      return None

  def nextChannel (self, searchChannel):
    for i in self.theChannels:
      print i.name
    
