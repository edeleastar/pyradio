import sys, tty, termios


def getch(mac):
  if mac:
    return raw_input()
  else:
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
      tty.setraw(sys.stdin.fileno())
      ch = sys.stdin.read(1)
    finally:
      termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch
  
def getUpDown():
  ch1 = getch(None)
  ch2 = getch(None)
  if ch1 == '[' and ch2 == 'A':
    return +1
  if ch1 == '[' and ch2 == 'B':
    return -1   
  if ch1 == '[' and ch2 == 'C':
    return +2   
  if ch1 == '[' and ch2 == 'D':
    return -2           