if __name__ == "__main__":
  from player import Player
  from channelmanager import ChannelManager
  from argparse import ArgumentParser
  from chario import getch
  from subprocess import Popen
  
  parser = ArgumentParser()
  parser.add_argument("--mac",   help="mac version", action="store_true")
  args = parser.parse_args()
  
  channels = ChannelManager()
  player = Player(args.mac)
  player.channelManager = channels;
  
  key = getch(args.mac)
  while key != 'x':
    if ord(key) == 27:
      player.doControl()
    if key == ' ':
      player.stop()
    else:
      channel = channels.lookupChannel(key)
      if channel != None:
        player.play(channel)
    key = getch(args.mac)  
    
  player.stop()
  key = getch(args.mac)
  if key == 't':
    Popen (["sudo", "halt"])
  